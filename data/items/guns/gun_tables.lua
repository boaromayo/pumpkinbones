--[[
Aim animations:
the hero needs two animations per gun: stopped and walking. The system will append "_stopped" or "_walking" to the end of the "aim_animation" value specified here.
--]]

return {
  example = {
    aim_animation = "gun_pistol",
    aim_sound = "gun_pistol",
    ammo_cost = 1,
    fire_sound = "gunshot_pistol",
    bullet_sprite = "hero_projectiles/bullet",
    bullet_model = "hero_projectiles/bullet", --this is the default model
    num_bullets = 1,
    spread = 0,
    projectile_properties = {
      damage = 45,
      speed = 450,
      range = 500,
      obstacle_collision_callback = function(bullet)
        local x, y, z = bullet:get_position()
        local map = bullet:get_map()
        map:create_explosion{x=x, y=y, layer=z}
      end,
      enemy_collision_callback = function(bullet, enemy)
        enemy:stagger(2000)
      end,
    },
  },

  ["guns/acorn"] = {
    ammo_cost = 1,
    fire_sound = "gun_acorn",
    bullet_sprite = "entities/acorn",
    num_bullets = 1,
    spread = 0,
    aim_animation = "aiming_pistol",
    aim_sound = "gun_aim_pistol",
    projectile_properties = {
      damage = 5,
      speed = 450,
      range = 500,
    },
    chamber_time = 100,
    chamber_sound = "gun_revolver_hammer_cock",
  },


}

