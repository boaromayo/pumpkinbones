local item = ...
local game = item:get_game()

function item:on_started()
  item:set_savegame_variable("possession_acorn_gun")
end

function item:on_obtaining()
  game:set_value("equipped_gun", "guns/acorn")
  game:add_bullets(10)
end

