local game_meta = sol.main.get_metatable"game"

function game_meta:get_max_bullets()
  local game = self
  return game:get_value("max_bullets") or 0
end


function game_meta:set_max_bullets(amount)
  local game = self
  return game:set_value("max_bullets", amount)
end


function game_meta:get_bullets()
  local game = self
  return game:get_value("bullets") or 0
end

function game_meta:set_bullets(amount)
  local game = self
  if amount > game:get_max_bullets() then amount = game:get_max_bullets() end
  if amount < 0 then amount = 0 end
  game:set_value("bullets", amount)
end

function game_meta:remove_bullets(amount)
  local game = self
  game:set_value("bullets", math.max(game:get_bullets() - amount, 0))
end

function game_meta:add_bullets(amount)
  local game = self
  game:set_value("bullets", math.min(game:get_bullets() + amount, game:get_max_bullets()))
end
