require("items/guns/bullets")

local manager = {}

local gun_tables = require("items/guns/gun_tables")
local reload_hud = require("items/guns/gun_reload_hud")

function manager:get_gun_tables()
  return gun_tables
end


local hero_meta = sol.main.get_metatable("hero")
local game_meta = sol.main.get_metatable("game")

local strafe_aim = false --logically has to default to false, sorry, set default with game:set_aim_style("strafe") or game:set_value("aim_style", "strafe")
local speed_slowdown = 30

local reticle = sol.sprite.create("hud/reticle")

local aim_state = sol.state.create("aiming")
aim_state:set_can_control_direction(true)
aim_state:set_can_control_movement(strafe_aim)
aim_state:set_gravity_enabled(true)
aim_state:set_can_come_from_bad_ground(true)
aim_state:set_can_be_hurt(true)
aim_state:set_can_use_sword(false)
aim_state:set_can_use_item(false)
aim_state:set_can_interact(false)
aim_state:set_can_grab(false)
aim_state:set_can_push(false)
aim_state:set_can_pick_treasure(true)
aim_state:set_can_use_teletransporter(true)
aim_state:set_can_use_switch(true)
aim_state:set_can_use_stream(true)
aim_state:set_can_use_stairs(true)
aim_state:set_can_use_jumper(true)
aim_state:set_carried_object_action("throw")
aim_state.aim_animation = "aiming_pistol" --default to avoid errors

local function set_strafe_aim(strafe_bool)
  strafe_aim = strafe_bool
  aim_state:set_can_control_direction(not strafe_aim)
  aim_state:set_can_control_movement(strafe_aim)
end

function game_meta:set_aim_style(style)
  local game = self
  assert((style == "strafe") or (style == "pivot"), "Aim style must be either 'strafe' or 'pivot'")
  game:set_value("aim_style", style)
end


-------------
--Aim Angle--
-------------
function hero_meta:set_aim_angle(new_angle)
  local hero = self
  aim_state.aim_angle = new_angle
  hero:on_aim_angle_changed(new_angle)
end

function hero_meta:get_aim_angle()
  local state_ob = self:get_state_object()
  assert(state_ob and state_ob:get_description() == "aiming", "The hero must be in an aiming state to call hero:get_aim_angle()")
  return aim_state.aim_angle or 0
end


function hero_meta:on_aim_angle_changed(new_angle)
  local hero = self
  local reticle_offset = 40
  local x, y, z = hero:get_position()
  local dx = math.cos(new_angle) * reticle_offset
  local dy = math.sin(new_angle) * reticle_offset
  reticle:set_xy(dx, dy * -1 - 8)
  reticle:set_rotation(new_angle)
end


-------------
--Aim State--
-------------

function aim_state:on_started()
  local hero = aim_state:get_entity()
  local game = hero:get_game()
  assert(game:get_value"equipped_gun", "The player should not be allowed to go into the aim state without an equipped gun")
  local equipped_gun = gun_tables[game:get_value("equipped_gun")]
  --Animation:
  aim_state.aim_animation = equipped_gun.aim_animation
  hero:set_animation(aim_state.aim_animation .. "_stopped")
  --Default aim angle to the direction the player is facing:
  hero:set_aim_angle(hero:get_direction() * math.pi / 2)
  --Slow Speed:
  hero:set_walking_speed(hero:get_walking_speed() - speed_slowdown)
  --Allow a little wiggle room to aim in a different direction when you start strafing
  sol.timer.start(aim_state, 200, function()
    aim_state:set_can_control_direction(not strafe_aim)
  end)

  --Because we do not have an on_mouse_moved() event, we'll hack it with a timer checking over and over
  sol.timer.start(aim_state, 0, function()
    --check to face the direction the mouse or right stick is pointed
    if game.mouse_aiming then
      aim_angle = hero:get_angle_to_mouse()
      local dir4 = sol.main.get_direction4(aim_angle)
      if hero:get_direction() ~= dir4 then
        hero:set_direction(dir4)
      end
      hero:set_aim_angle(aim_angle)
    end
    return 30
  end)
end


function aim_state:on_finished()
  local hero = aim_state:get_entity()
  hero:set_walking_speed(hero:get_walking_speed() + speed_slowdown)
  sol.audio.play_sound("gun_lower")
end


local function set_hero_direction(hero)
  local game = sol.main.get_game()
  dir8 = sol.controls.get_main_controls():get_direction() or hero:get_direction() * 2
  hero:set_direction(dir8 / 2)
  hero:set_aim_angle(dir8 * math.pi / 4)
end


function aim_state:on_command_pressed(command)
  local hero = aim_state:get_entity()
  local game = sol.main.get_game()
  local direction = 0

  if command == "attack" and not hero.fire_cooldown then
    hero:fire()
  elseif not strafe_aim and (command == "right" or command == "up" or command == "left" or command == "down") then
    set_hero_direction(hero)
  end
end


function aim_state:on_command_released(command)
  local hero = aim_state:get_entity()
  local handled = false
  if command == "aim" then
    hero:unfreeze()
    handled = true
  elseif not strafe_aim and (command == "right" or command == "up" or command == "left" or command == "down") then
    set_hero_direction(hero)
  end
  return handled
end


--Aim with right joystick:
if sol.main.old_controls_version then --Solarus <= 1.6 compatibility, hardcoded to Xbox axis numbers:
  function aim_state:on_joypad_axis_moved(axis, state)
    if sol.main.get_game():get_value("option_disable_right_stick_aiming") then return end --allow to disable right stick aiming with a savegame value
    if sol.main.get_axis_direction(3, 4) ~= -1 then
      local hero = aim_state:get_entity()
      direction4 = sol.main.get_axis_direction(3, 4) / 2 --direction is returned 0-7, so divide to get direction4
      hero:set_direction(direction4)
      hero:set_aim_angle(direction4 * math.pi / 2)
    end
  end

else
  function aim_state:on_axis_moved(command_axis, state, command_ob)
    if sol.main.get_game():get_value("option_disable_right_stick_aiming") then return end --allow to disable right stick aiming with a savegame value
    if command_axis == "aim_x" or command_axis == "aim_y" then
      local AIM_DEADZONE = .2
      local hero = aim_state:get_entity()
      local x = sol.controls.get_main_controls():get_axis_state("aim_x")
      local y = sol.controls.get_main_controls():get_axis_state("aim_y")
      if (math.abs(x) < AIM_DEADZONE and math.abs(y) < AIM_DEADZONE) then return end
      local angle = math.atan2(x, y) - math.pi / 2
      local dir4 = sol.main.get_direction4(angle)
      hero:set_direction(dir4)
      hero:set_aim_angle(angle)
    end
  end
end


function aim_state:on_movement_changed(m)
  local hero = aim_state:get_entity()
  --check if we should play walking or stopped animation
  if m:get_speed() > 0 then
    hero:set_animation(aim_state.aim_animation .. "_walking")
  else
    hero:set_animation(aim_state.aim_animation .. "_stopped")
  end
end


function aim_state:on_pre_draw(camera)
  local hero = aim_state:get_entity()
  local x, y, z = hero:get_position()
  local map = hero:get_map()
  map:draw_visual(reticle, x, y)
end


----------------
--Hero Actions--
----------------

function hero_meta:start_aiming()
  local hero = self
  local game = hero:get_game()

  set_strafe_aim(game:get_value("aim_style") == "strafe")

  local state_ob = hero:get_state_object()
  if state_ob and state_ob:get_description() == "aiming" then
    return
  end

  if not game:get_value("equipped_gun") then
    sol.audio.play_sound"wrong"
    return
  end

  local equipped_gun = gun_tables[game:get_value("equipped_gun") or "example"]
  local aim_sound = equipped_gun.aim_sound or "gun_aim_pistol"

  hero:start_state(aim_state)
  sol.audio.play_sound(aim_sound)

end


function hero_meta:fire()
  local hero = self
  local game = hero:get_game()
  local map = hero:get_map()

  local equipped_gun = gun_tables[game:get_value("equipped_gun")]
  local gun_item = game:get_item(game:get_value("equipped_gun"))
  local ammo_amount_needed = equipped_gun.ammo_cost or 1
  local fire_sound = equipped_gun.fire_sound or "gunshot_pistol"
  local num_bullets = equipped_gun.num_bullets or 1
  local bullet_model = equipped_gun.bullet_model or "hero_projectiles/bullet"
  local bullet_sprite = equipped_gun.bullet_sprite or "hero_projectiles/bullet"
  local spread = math.rad(equipped_gun.spread or 0)
  local projectile_properties = equipped_gun.projectile_properties or {} --sets at least damage, speed, and range
  local chamber_time = equipped_gun.chamber_time or 100
  local chamber_sound = equipped_gun.chamber_sound
  local chamber_sound_delay = equipped_gun.chamber_sound_delay or 100

  if game:get_bullets() < ammo_amount_needed then
    sol.audio.play_sound("gun_empty_acorn")
    return
  end

  --If item has a fire override
  --NOTE: a fire override must consider the fact that the hero is still in the aim state during firing
  if gun_item.fire_override then
    gun_item:fire_override(equipped_gun)
    return
  end

  --spend bullets and fire
  game:remove_bullets(ammo_amount_needed)
  local x, y, z = hero:get_position()
  local aim_angle = hero:get_aim_angle()

  for i = 1, num_bullets do
    local angle = aim_angle - (spread / 2) + spread / (num_bullets) * i
    local dx = math.cos(angle) * 8
    local dy = math.sin(angle) * 8

    local projectile = map:create_custom_entity{
      x = x + dx,
      y = y + dy,
      layer = z,
      direction = 0,
      width = 8, height = 8, 
      sprite = bullet_sprite,
      model = "hero_projectiles/bullet",
    }
    projectile:set_origin(4, 5)
    for k, v in pairs(projectile_properties) do
      projectile[k] = v
    end
    projectile:set_can_traverse("hero", true)


    local facing_direction = sol.main.get_direction4(aim_angle)
    hero:set_direction(facing_direction)
    projectile:shoot(angle)

    --[[Bullet Particle effects:
    local bul_em = map:create_particle_emitter(x, y, z)
    bul_em.target = projectile
    bul_em.particle_sprite = "effects/spark_particle"
    bul_em.width, bul_em.height = 4, 4
    bul_em.frequency = 30
    bul_em.particle_distance = 8
    bul_em.particle_speed = 90
    bul_em.particle_opacity = {250,255}
    --bul_em.particle_color = {255,240,0}
    bul_em.angle = angle + math.pi
    bul_em.angle_variance = math.rad(3)
    bul_em:emit()
    --]]
  end

  --Play sound
  sol.audio.play_sound(fire_sound)
  if gun_item.on_using then
    gun_item:on_using()
  end


  --Cooldown / chamber next bullet
  sol.timer.start(hero, chamber_sound_delay, function()
    if chamber_sound then sol.audio.play_sound(chamber_sound) end
  end)
  hero.fire_cooldown = true
  sol.timer.start(hero, chamber_time, function() hero.fire_cooldown = false end)
  --Reloading bar:
  if chamber_time >= 400 then
    reload_hud:set_duration(chamber_time)
    sol.menu.start(game, reload_hud)
  end
  

end


function hero_meta:recoil(amount)
  local hero = self
  local game = hero:get_game()
  local direction = hero:get_direction()
  amount = amount or 8

  direction = (direction + 2) % 4
  local m = sol.movement.create"straight"
  m:set_angle(direction * math.pi / 2)
  m:set_speed(120)
  m:set_max_distance(amount)
  local function finish()
    hero:unfreeze()
  end
  m:start(hero, function() finish() end)
  function m:on_obstacle_reached() finish() end
end



return manager