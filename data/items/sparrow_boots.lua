local item = ...
local game = item:get_game()


function item:on_started()
  item:set_savegame_variable("possession_lantern")
  item:set_assignable(true)
end


function item:on_obtaining()
  game:set_value("dash_level", 2)
end

