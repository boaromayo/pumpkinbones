local item = ...

function item:on_created()
  self:set_shadow("small")
  self:set_can_disappear(true)
  self:set_brandish_when_picked(false)
end

function item:on_started()
  self:set_obtainable(self:get_game():has_item("firecracker"))
end

function item:on_obtaining(variant, savegame_variable)
  local amounts = {1, 3, 5}
  local amount = amounts[variant]
  if amount == nil then
    error("Invalid variant '" .. variant .. "' for item 'drops/firecracker'")
  end
  self:get_game():get_item("firecracker"):add_amount(amount)
end
