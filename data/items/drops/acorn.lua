--[[
local item = ...
local game = item:get_game()

function item:on_created()
  self:set_shadow("small")
  self:set_can_disappear(true)
  self:set_brandish_when_picked(false)
end


function item:on_started()
print"setting obtainable"
  item:set_obtainable(game:has_item("guns/acorn"))
print("Is obtainable?", item:is_obtainable())
end


function item:on_obtaining(variant, savegame_variable)
  game:add_bullets(1)
end
--]]


local item = ...

function item:on_created()

  self:set_shadow("small")
  self:set_can_disappear(true)
  self:set_brandish_when_picked(false)
end

function item:on_started()

  -- Disable pickable arrows if the player has no bow.
  -- We cannot do this from on_created() because we don't know if the bow
  -- is already created there.
  self:set_obtainable(self:get_game():has_item("guns/acorn"))
end

function item:on_obtaining(variant, savegame_variable)
  local amounts = {1, 3, 5, 10}
  local amount = amounts[variant]
  if amount == nil then
    error("Invalid variant '" .. variant .. "' for item 'drops/acorn'")
  end
  self:get_game():add_bullets(1)
end


