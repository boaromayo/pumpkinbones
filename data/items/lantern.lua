local item = ...
local game = item:get_game()


function item:on_started()
  item:set_savegame_variable("possession_lantern")
  item:set_assignable(true)
end


function item:on_obtaining()
  game:set_item_assigned(2, item)
end


function item:on_using()
  local map = item:get_map()
  local hero = map:get_hero()
  hero:set_animation("lantern_raising")
  sol.timer.start(hero, 200, function()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    local offset = 24
    local dx = {[0] = offset, [1] = 0, [2] = offset * -1, [3] = 0}
    local dy = {[0] = 0, [1] = offset * -1, [2] = 0, [3] = offset}
    map:create_fire({
      x = x + dx[direction],
      y = y + dy[direction],
      layer = z,
    })
    sol.audio.play_sound("fireball")
    sol.timer.start(100, function()
      hero:set_animation("lantern_lowering", function()
        hero:set_animation("stopped")
        item:set_finished()
      end)
    end)
  end)
end

