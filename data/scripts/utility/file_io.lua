local manager = {}


function manager.save_data(data, file_name)
  local file = sol.file.open(file_name, "w")

	for key,value in pairs(data) do
		if type(value)=="string" then
			value = string.format("%q", value) --add quotes and escape characters to string
		else value = tostring(value) end
		file:write(string.format("%s = %s\n", key, value))
	end
	
	file:flush()
	file:close()
end


function manager.read_data(file_name)
  local data = {}

  	if sol.file.exists(file_name) then
  		local env = setmetatable({}, {__newindex = function(self, key, value)
  			data[key] = value
  		end})
  		
  		local chunk = sol.main.load_file(file_name)
  		setfenv(chunk, env)
  		chunk()
  else
    error("File does not exist: " .. file_name)
  	end

  return data
end


return manager
