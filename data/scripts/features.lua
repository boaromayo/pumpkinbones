-- Sets up all non built-in gameplay features specific to this quest.

local features = {}

require("items/guns/gun_manager")
require("scripts/action/dash_manager")
require("scripts/controls/controls_manager")
require("scripts/elements/enemy_elemental_meta")
require("scripts/elements/hero_elemental_meta")
require("scripts/elements/map_elemental_meta")
require("scripts/hud/hud")
require("scripts/menus/dialog_box")
require("scripts/menus/pause")
require("scripts/meta/camera")
require("scripts/meta/enemy")
require("scripts/meta/hero")
require("scripts/meta/switch")
require("scripts/utility/angle_utility")

--Some scripts need a game passed to initialize. features.init(game) is called by the game manager, so those scripts are initialized here:
function features.init(game)
  require("scripts/button_inputs"):initialize(game)
end

return features
