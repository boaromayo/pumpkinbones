--[[
Controls Manager
Created by Max Mraz, licensed MIT
Sets up defaults controls for Solarus 1.7+ games

--]]

--This won't work under Solarus 1.7, error if you try to call it
if tonumber(sol.main.get_solarus_version():match("(%d+.%d+)")) < 1.7 then
  print("Cannot use controls manager with Solarus version < 1.7")
  return
end

local manager = {}
local file_io = require("scripts/utility/file_io")
local menu_controls_manager = require("scripts/controls/menu_controls_manager")


--List of all commands the game can use:
local commands = require("scripts/controls/commands")

--These are the files that will be saved/read to the savegame directory
local control_files = {
  keyboard = "controls_keyboard.dat",
  joypad = "controls_joypad.dat",
}

--Disable legacy joypad support:
sol.input.set_joypad_enabled(false)
--Enable analog controls (omnidirectional movement):
--sol.controls.set_analog_commands_enabled(true) --NOTE: this is commented out because it causes a bug where Left/Right on keyboard controls move the hero in reverse if enabled while player is using keyboard


--Enable Rumble:
require("scripts/controls/rumble")


function sol.controls.get_controls_manager()
  return manager
end


function sol.controls.get_main_controls()
  return sol.main.main_controls
end


function sol.controls.get_menu_controls()
  return sol.main.menu_controls
end


function manager.init()
  --Load mappings from files/defaults:
  manager.load_default_control_files()
  local first_joypad = sol.input.get_joypads()[1]
  manager.create_controls(first_joypad)
end


function manager.load_default_control_files()
  --If control file does not exist, create it by reading defaults
  --Reads from either "save_directory/controls_{control_type}.dat" (if it exists)
  --or defaults to a table from "scripts/controls/defaults_{control_type}"
  for control_type, file in pairs(control_files) do
    if not sol.file.exists(file) then
      local data = require("scripts/controls/defaults_" .. control_type)
      file_io.save_data(data, file)
    end
  end
end


function manager.reset_default_controls()
  print("Resetting default controls")
  local active_joypad = sol.main.main_controls.joypad
  for control_type, file in pairs(control_files) do
    local data = require("scripts/controls/defaults_" .. control_type)
    file_io.save_data(data, file)
    manager.create_controls(active_joypad)
  end
end


function manager.clear_control_bindings(controls_ob)
  for _, command in ipairs(commands) do
    controls_ob:set_keyboard_binding(command, nil)
    controls_ob:set_joypad_binding(command, nil)
  end
end


function manager.get_control_data(control_type)
  --Returns commands and bindings as key value pairs from the control files in the savegame directory
  local file = control_files[control_type]
  assert(file, "No control file found for control type" .. control_type .. " -- were default values not loaded?")
  local data = file_io.read_data(file)
  return data
end


function manager.create_controls(joypad)
  --Remove any old controls:
  if sol.main.main_controls then sol.main.main_controls:remove() end

  if sol.main.get_type(joypad) == "joypad" then
    print("Creating new joypad controls - joypad:", joypad:get_name())
    sol.controls.set_analog_commands_enabled(true)
    --Create primary controls:
    sol.main.main_controls = sol.controls.create_from_joypad(joypad)
    sol.main.main_controls.joypad = joypad
    sol.main.main_controls.active_device = joypad
  else
    print("Creating new keyboard controls")
    --Create primary controls:
    sol.controls.set_analog_commands_enabled(false)
    sol.main.main_controls = sol.controls.create_from_keyboard()
    sol.main.main_controls.active_device = "keyboard"
  end
  manager.load_control_mapping()
  manager.set_game_controls()
end


function manager.load_control_mapping()
  --Reads keyboard and joypad bindings and sets sol.main.main_controls
  for control_type, file_name in pairs(control_files) do
    local data = manager.get_control_data(control_type)

    --Set joypad bindings:
    if control_type == "joypad" then
      for command, mapping in pairs(data) do
        if command:match("^movement_stick") then
          sol.main.main_controls:set_joypad_axis_binding("X", mapping .. "_x")
          sol.main.main_controls:set_joypad_axis_binding("Y", mapping .. "_y")
        elseif command:match("^aim_stick") then
          sol.main.main_controls:set_joypad_axis_binding("aim_x", mapping .. "_x")
          sol.main.main_controls:set_joypad_axis_binding("aim_y", mapping .. "_y")
        else
          sol.main.main_controls:set_joypad_binding(command, mapping)
        end
      end

    --Set keyboard bindings:
    elseif control_type == "keyboard" then
      for command, mapping in pairs(data) do
          sol.main.main_controls:set_keyboard_binding(command, mapping)
      end
      --Set axis manually, lol why the fuck do I need to do this?
      local left_key, right_key = data.left, data.right
      sol.main.main_controls:set_keyboard_axis_binding("X", left_key, right_key)

    end

    file_io.save_data(data, file_name)

  end

  menu_controls_manager.create_menu_controls()
end


--Apply controls when game starts:
local game_meta = sol.main.get_metatable"game"
game_meta:register_event("on_started", function(game)
  --Old code (what we want to use):
  --manager.set_game_controls()

  --Workaround for now: addresses issue where when you die, controls are removed when the hero dies and then you can't move after death lol
  local previous_device = sol.main.main_controls and sol.main.main_controls.active_device
  sol.main.main_controls:remove()
  manager.create_controls(previous_device)
  manager.set_game_controls()
end)


function manager.set_game_controls()
  local game = sol.main.get_game()
  if game then
    assert(sol.main.main_controls:get_joypad_binding"action" or sol.main.main_controls:get_keyboard_binding("action"), "No valid keybinding found for 'action' in main controls, sol.main.main_controls is somehow instantiated incorrectly")
    local hero = game:get_hero()
    --I don't think the below IF block is ever called since right now we need to totally recreate the controls upon a game over
    if (game:get_controls() == sol.main.main_controls) or (hero:get_controls() == sol.main.main_controls) then
      --Controls already set, this was probablu just called after gameover, not a new game setup
      return
    end
    game:get_controls():remove()
    hero:get_controls():remove()
    game:set_controls(sol.main.main_controls)
    hero:set_controls(sol.main.main_controls)
  end
end



--These functions are called when keyboard or joypad input is pressed, makes any changes necessary to hotswap to other control device:
function manager.activate_keyboard()
  if sol.main.main_controls.active_device == "keyboard" then return end --skip if keyboard already active
  sol.main.main_controls.active_device = "keyboard"
  --Disable analog controls for some reason this causes Left/Right to move the player in reverse
  sol.controls.set_analog_commands_enabled(false)
end


function manager.activate_joypad(joypad)
  if (sol.main.main_controls.active_device == joypad) and (sol.main.main_controls.joypad == joypad) then return end --skip if this joypad is already active
  sol.timer.start(sol.main, 10, function()
    joypad:rumble(1, 500)
  end)
  sol.main.main_controls:set_joypad(joypad)
  sol.main.main_controls.active_device = joypad
  sol.main.main_controls.joypad = joypad
  menu_controls_manager.update_joypad(joypad)
  --Enable analog controls, causes some bug with left/right movement on keyboard when enabled
  sol.controls.set_analog_commands_enabled(true)
end


--NOTE: probably don't use this. If two commands swapped, you won't know, and can only save one when two were actually changed:
function manager.save_binding(control_type, command, binding)
  print("Warning, this probably isn't safe to use less you're SURE there aren't duplicates because duplicates won't work")
  local file_name = control_files[control_type]
  local data = file_io.read_data(file_name)
  data[command] = binding
  file_io.save_data(data, file_name)
end


function manager.save_bindings()
  for control_type, file_name in pairs(control_files) do
    local data = file_io.read_data(file_name)
    for _, command in ipairs(commands) do
      if control_type == "keyboard" then
        data[command] = sol.main.main_controls:get_keyboard_binding(command)
      elseif control_type == "joypad" then
        data[command] = sol.main.main_controls:get_joypad_binding(command)
      end
    end
    file_io.save_data(data, file_name)
  end
end


--When keyboard/joypad input is detected, active the corresponding input type:
function sol.main:on_key_pressed(key)
  manager.activate_keyboard()
end

function sol.main:on_joypad_button_pressed(button, joypad)
  manager.activate_joypad(joypad)
end

function sol.input:on_joypad_connected(joypad)
  manager.activate_joypad(joypad)
end


--Functions for other systems to call:
--Returns which console the controller belongs to, useful for displaying the appropriate button sprites:
function manager.get_joypad_type()
  local controls_ob = sol.controls.get_main_controls()
  if not controls_ob.joypad then
    return nil
  end
  local name = controls_ob.joypad:get_name():lower()
  if name:match("xbox") then
    return "xbox"
  elseif name:match("playstation") then
    return "playstation"
  elseif name:match("switch") then
    return "switch"
  else
    return "generic"
  end
end


manager.init() --init yourself to set up controls objects, to allow navigating title screen menus


return manager
