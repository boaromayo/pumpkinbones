--[[
A note on how to set command bindings in 1.7+, putting it here because I'm not sure where else to:

If you want to set which stick is for axis movement, you have to use:
      sol.controls.get_main_controls():set_joypad_axis_binding("X", "left_x")
"set_joypad_axis_binding" is exclusively for analog axis movement - i.e., how you make the player walk -, from what I can tell
If you want to put any other command onto an axis (which includes the triggers), then you don't use "set_joypad_axis_binding",
you use "set_joypad_binding" AND you must include a + or - to indicate the axis movemet direction. For example:
      sol.controls.get_main_controls():set_joypad_binding("aim", "trigger_left +")

--]]

return {
  action = "a",
  attack = "right_shoulder",
  pause = "start",
  item_1 = "x",
  item_2 = "y",

  --Movement axis:
  movement_stick = "left",
  aim_stick = "right",

  --custom_commands
  aim = "left_shoulder",
  --aim = "trigger_left +",
  dodge = "b",
}
