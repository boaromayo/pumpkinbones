--[[
Created by Max Mraz, licensed MIT
--]]

local game_meta = sol.main.get_metatable"game"

function game_meta:rumble(amount, duration)
  local game = self
  if sol.main.old_controls_version then return end -- solarus <= 1.6 doesn't support rumble
  local joypad = sol.controls.get_main_controls().joypad
  if not joypad then return end

  --Presets
  if type(amount) == "string" then
    if amount == "melee_hit" then
      amount, duration = .99, 1000
    else
      error("Invalid rumble preset passed to game:rumble(preset)")
    end
  end

  if joypad:has_rumble() then
    joypad:rumble(amount, duration)
  end
end
