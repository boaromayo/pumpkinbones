return {
  up = "up",
  down = "down",
  left = "left",
  right = "right",
  action = "return",
  attack = "c",
  pause = "d",
  item_1 = "x",
  item_2 = "v",

  --custom commands
  dodge = "space",
  aim = "z",
} --]]
