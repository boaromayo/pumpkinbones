--[[
Created by Max Mraz, Licensed MIT
Functions to create a controls object responsible for managing menu input

This menu controls object will have all its input captured and used to simulate commands on the main controls object, if the bindings don't already match for that command
--]]

local manager = {}
local game_meta = sol.main.get_metatable"game"

local menus_accept_action = true --change this to false if menus will only listen for "confirm" and not "action"

local command_adjustments = {
  confirm_2 = "confirm",
  cancel_2 = "cancel",
}


--Create a controls object for sending input to menus:
function manager.create_menu_controls()
  local controls_manager = sol.controls.get_controls_manager()
  local main_controls = sol.controls.get_main_controls()
  if not main_controls then error("Main controls must be created before menu controls") end
  local joypad = main_controls:get_joypad()
  if sol.main.menu_controls then sol.main.menu_controls:remove() end

  local control_ob
  if joypad then
    control_ob = sol.controls.create_from_joypad(joypad)
  else
    control_ob = sol.controls.create_from_keyboard()
  end
  controls_manager.clear_control_bindings(control_ob)
  sol.main.menu_controls = control_ob
  manager.load_controls_mapping()
end


--Load the bindings onto the menu controls objet:
function manager.load_controls_mapping()
  local controls_ob = sol.main.menu_controls
  local default_bindings = require("scripts/controls/defaults_menu")
  
  for control_type, data in pairs(default_bindings) do
    for command, binding in pairs(data) do
      if control_type == "keyboard" then
        controls_ob:set_keyboard_binding(command, binding)

      elseif control_type == "joypad" then
        if command:match("^movement_stick") then
          controls_ob:set_joypad_axis_binding("X", binding .. "_x")
          controls_ob:set_joypad_axis_binding("Y", binding .. "_y")
        else
          controls_ob:set_joypad_binding(command, binding)
        end
      end
    end
  end
end


function manager.update_joypad(joypad)
  sol.main.menu_controls:set_joypad(joypad)
end




--Read all commands: if they're from the menu controls object, and the main controls object isn't already reading that command
local menu_command_capture_menu = {}
function menu_command_capture_menu:on_command_pressed(command, controls_ob)
  --print("Game level - command pressed:", command)
  --print("Controls ob is", controls_ob, (controls_ob == sol.main.main_controls and "main") or (controls_ob == sol.main.menu_controls and "menu") or "unknown")
  local handled = false

  if controls_ob == sol.controls.get_menu_controls() then
    --Adjust command:
    adjusted_command = command_adjustments[command] or command
    local main_controls = sol.controls.get_main_controls()
    local menu_controls = controls_ob

    --See if bindings overlap
    local overlapping = false
    if main_controls.active_device == "keyboard" then
      local key_pressed = menu_controls:get_keyboard_binding(command)
      if key_pressed == main_controls:get_keyboard_binding(adjusted_command) then overlapping = true end
      --also overlap if this is a "confirm" key but also the main controls' "action" key
      if menus_accept_action then
        if (adjusted_command == "confirm") and (main_controls:get_keyboard_binding("action") == key_pressed) then overlapping = true end
      end
    else
      local button_pressed = menu_controls:get_joypad_binding(command)
      if button_pressed == main_controls:get_joypad_binding(adjusted_command) then overlapping = true end
      --also overlap if this is a "confirm" key but also the main controls' "action" key
      if menus_accept_action then
        if (adjusted_command == "confirm") and (main_controls:get_joypad_binding("action") == button_pressed) then overlapping = true end
      end
    end

    if not overlapping then
      main_controls:simulate_pressed(adjusted_command)
    end

    handled = true --we always handle menu commands here to prevent them from continuing
  end

  return handled
end
sol.menu.start(sol.main, menu_command_capture_menu)


return manager
