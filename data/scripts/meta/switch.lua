local switch_meta = sol.main.get_metatable"switch"


function switch_meta:on_activated()
  local switch = self
  local map = self:get_map()

  local door_prefix = switch:get_property("door_prefix")
  if door_prefix then
    map:open_doors(door_prefix)
  end

end


function switch_meta:toggle()
  local switch = self
  if not switch:is_activated() then
    sol.audio.play_sound("switch")
    switch:set_activated(true)
    switch:on_activated()
  else
    sol.audio.play_sound("switch")
    switch:set_activated(false)
    if switch.on_inactivated then switch:on_inactivated() end
  end
end


