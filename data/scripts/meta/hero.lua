local hero_meta = sol.main.get_metatable"hero"

hero_meta:register_event("on_created", function(hero)
  hero:set_tunic_sprite_id("hero/skeleton_man")
end)


function hero_meta:step_forward(distance, speed)
  local hero = self
  local map = self:get_map()
  distance = distance or 8
  speed = speed or 90
  local speed_mod = (distance - 24) * 4
  if distance > 24 then speed = speed + speed_mod end
  local angle = hero:get_direction() * math.pi / 2
  local x, y, z = hero:get_position()
  local is_obstacle = false
  for i = 0, distance do
    dx = math.cos(angle) * i
    dy = math.sin(angle) * i * -1
    local ground = map:get_ground(x + dx, y + dy, z)
    if hero:test_obstacles(dx, dy) or ground == "deep_water" or ground == "shallow_water" or ground == "hole" or ground == "lava" then
      is_obstacle = true
      distance = math.max(0, i - 4)
      break
    end
    local enemy_catch_w, enemy_catch_h = 16, 16
    --[[
    for ent in map:get_entities_in_rectangle(x + dx - (enemy_catch_w / 2), y + dy - (enemy_catch_h / 2), enemy_catch_w, enemy_catch_h) do
      if ent:get_type() == "enemy" then
        is_obstacle = true
        distance = math.max(0, i - 4)
        break
      end
    end
    --]]
  end
  local capture_dist = 8
  local rect_dx = {[0] = 0, [1] = -4, [2] = capture_dist * -1, [3] = -4}
  local rect_dy = {[0] = -4, [1] = capture_dist * -1, [2] = -4, [3] = 0}
  local direction = hero:get_direction()
  local is_horiz = ((direction == 0) or (direction == 2)) and true or false
  local rect_width, rect_height = 8, 8
  if is_horiz then rect_width = capture_dist
  else rect_height = capture_dist end
  for ent in map:get_entities_in_rectangle(x + rect_dx[direction], y + rect_dy[direction], rect_width, rect_height) do
    if ent:get_type() == "enemy" then
      is_obstacle = true
      distance = 0
    end
  end

  if distance < 4 then return end
  local m = sol.movement.create("straight")
  m:set_angle(angle)
  m:set_speed(speed)
  m:set_max_distance(distance)
  m:start(hero)
end
