local game_meta = sol.main.get_metatable"game"
local button_mapping_menu = require"scripts/menus/button_mapping"


function game_meta:on_paused()
  local game = self
  if not sol.menu.is_started(button_mapping_menu) then
    sol.menu.start(game, button_mapping_menu)
  end
end


function game_meta:on_unpaused()
  if sol.menu.is_started(button_mapping_menu) then
    sol.menu.stop(button_mapping_menu)
  end
end

